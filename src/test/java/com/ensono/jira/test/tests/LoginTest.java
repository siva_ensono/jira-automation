package com.ensono.jira.test.tests;

import com.ensono.jira.bean.User;
import com.ensono.jira.constants.Outcome;
import com.ensono.jira.page.HomePage;
import com.ensono.jira.page.LoginPage;
import com.ensono.autoframework.utils.ConfigUtil;
import com.ensono.autoframework.utils.DriverManager;

public class LoginTest{	
	private LoginPage loginPage = new LoginPage(DriverManager.getDriver()); 
	private HomePage homePage;
	private User userExp = new User();
	
	public void open_login_page(){	
		loginPage.loadPage(ConfigUtil.getInstance().getConfig().getURL());
		loginPage.isDisplayedCheck();
	}

	public void enter_username(String username) {
		userExp.setUserName(username);
		loginPage.EnterUsername(userExp);
	}

	public void enter_password(String password) {
		userExp.setPassword(password);
		loginPage.EnterPassword(userExp);
	}
	

	public void is_loggedin(String outcomeString){
		Outcome outcome = Outcome.outcomeForName(outcomeString);
		switch(outcome){
			case SUCCESS:		
				homePage = loginPage.LoginAsValidUser(userExp);
				homePage.isDisplayedCheck();
			break;
			case FAILURE:
				loginPage = loginPage.LoginAsInvalidUser(userExp);
				loginPage.checkLoginErrors();
			break;
		}
	}
	

}
