package com.ensono.jira.test.runners;

import cucumber.api.testng.TestNGCucumberRunner;
import cucumber.api.CucumberOptions;
import org.testng.annotations.Test;
import com.ensono.autoframework.runners.BaseCukesTest;
import com.ensono.autoframework.utils.ConfigUtil;

@CucumberOptions(
        features = {"src/test/resources/features"}, //path to the features
        glue = {"classpath:com.ensono.jira.test.stepDefinitions"}, //path to step definitions
        plugin = {"json:target/firefox2.json"}, //what formatters to use
        tags = {"@smokeN"} //what tags to include(@)/exclude(@~)
        ) 
public class CukesFirefoxTest2 extends BaseCukesTest{
    /**
     * Create one test method that will be invoked by TestNG and invoke the
     * Cucumber runner within that method.
     */
        
    @Test(groups = "cucumber2", description = "Using TestNGCucumberRunner to invoke Cucumber")
    public void runCukes() {    
    	
    	LOGGER.info("Starting Cuke Test in ..." + ConfigUtil.getInstance().getConfig().getBrowser()); 
        new TestNGCucumberRunner(getClass()).runCukes();
    }



    
}

